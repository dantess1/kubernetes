# from flask import Flask

# app = Flask(__name__)

# @app.route('/')
# def hello_world():
#     return '<h1 style="text-align: center; font-weight: bold; text-transform: uppercase;">Hello World v2</h1>'

# if __name__ == '__main__':
#     app.run(host='0.0.0.0', port=32777)

from flask import Flask, render_template
# from config import Config

app = Flask(__name__)
# app.config.from_object(Config)

@app.route('/')
def index():
    return render_template('index.html', title='Welcome to My Flask App!')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=32777)